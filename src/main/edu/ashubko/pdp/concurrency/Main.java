package edu.ashubko.pdp.concurrency;

public class Main {

    public static void main(String[] args) {
        AsyncStatisticsService asyncService = new AsyncStatisticsService();
        asyncService.generateStats();
    }
}
