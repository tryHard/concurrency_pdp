package edu.ashubko.pdp.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class AsyncStatisticsService {

    RandomService randomService = new RandomService();

    public void generateStats() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        List<MyCallable> callableTasks = new ArrayList<>();
        final int numb = 100;
        for (int i = 0; i < numb; i++) {
            callableTasks.add(new MyCallable());
        }

        List<Future<Boolean>> futures = new ArrayList<>();
        try {
            futures = executorService.invokeAll(callableTasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int pos = 0;
        int neg = 0;
        try {
            for (Future<Boolean> future : futures) {
                if (future.get()) {
                    pos++;
                } else {
                    neg++;
                }
            }
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("Number of calls: " + numb + " True: " + pos + " False: " + neg);
        return;
    }

    class MyCallable implements Callable<Boolean> {
        @Override
        public Boolean call() {
            return randomService.getRandom();
        }
    }
}
